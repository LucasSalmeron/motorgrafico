#include <Windows.h>
using namespace std;
class Graphics {


public:
	Graphics();
	bool Initialize();
	bool Shutdown();
	void Clear();
	void Begin();
	void End();
	void Present();
	bool SetupScene();
};