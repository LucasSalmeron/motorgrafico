using namespace std;
#include <windows.h>
#include <string>

class Window {
private:
	HWND hWnd;
	wstring		className;
public:
	bool createWindow(HINSTANCE hInstance, wstring title, int width, int height);
	void registerClass(HINSTANCE hInstance);
	Window(wstring className);
};
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);