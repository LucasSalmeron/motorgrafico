#include "Game.h"
#include "Graphics.h"
Game::Game() {
}

bool Game::Initialize(HINSTANCE hInstance) {
	static MSG	msg;			// Estructura de mensajes
	static bool	done = false;	// Si debo interrumpir el loop
	wstring className = L"nombreClase";
	Window window = Window(className);
	// Registro la clase
	window.registerClass(hInstance);
	//registerClass(hInstance);

	// Creo la Window
	window.createWindow(hInstance,
		L"El mejor titulo",
		800, 600);



	

	return true;
}

void Game::Loop() {
	static MSG	msg;			// Estructura de mensajes
	static bool	done = false;	// Si debo interrumpir el loop

	// Loop principal
	while (!done)
	{
		if (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE)) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);

			if (msg.message == WM_QUIT)
				done = true;			// ALT-F4
		}
	}
}
bool Game::Shutdown() {
	return true;
}