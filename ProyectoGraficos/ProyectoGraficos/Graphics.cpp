#include "Graphics.h"
Graphics::Graphics() {


}

void Graphics::Present() {



}
bool Graphics::SetupScene()
{
	HRESULT hRes;

	// Obtengo el viewport
	hRes = _pDevice->GetViewport(&_viewport);

	if (FAILED(hRes)) {
		Error::setError(5);
		return false;
	}

	// Registro el viewport obtenido
	//FileLog::Write("Viewport {x=%d, y=%d, w=%d, h=%d}\n", _viewport.X, _viewport.Y, _viewport.Width, _viewport.Height);

	// -----------------------------------------------------------------------
	// Fijo la Matriz del Mundo, que es la que transforma de coordenadas
	// locales de los objetos a la de mundo
	// -----------------------------------------------------------------------
	D3DXMATRIX d3dmat;
	D3DXMatrixIdentity(&d3dmat);
	_pDevice->SetTransform(D3DTS_WORLD, &d3dmat);

	// -----------------------------------------------------------------------
	// Fijo la matriz de vista, que es la transforma a todos los v�rtices
	// para cambiar el punto de visi�n de la escena
	// -----------------------------------------------------------------------
	Vector3 eyePos(_viewport.Width / 2.0f, _viewport.Height / 2.0f, -5.0f);
	Vector3 lookPos(_viewport.Width / 2.0f, _viewport.Height / 2.0f, 0.0f);
	Vector3 upVec(0.0f, 1.0f, 0.0f);
	D3DXMatrixLookAtLH(&d3dmat, &eyePos, &lookPos, &upVec);
	_pDevice->SetTransform(D3DTS_VIEW, &d3dmat);

	// -----------------------------------------------------------------------
	// Seteo la Matriz de Proyecci�n, que es la que transforma nuestro mundo
	// 3d en un viewport 2d que es mostrado en pantalla
	// -----------------------------------------------------------------------
	D3DXMATRIX mProjectionMatrix;
	float fAspectRatio = (float)_viewport.Width / _viewport.Height;

	//D3DXMatrixPerspectiveFovLH(&mProjectionMatrix, D3DX_PI/4.0f, fAspectRatio, 1.0f, 1000.0f);
	D3DXMatrixOrthoLH(&mProjectionMatrix, (float)_viewport.Width, (float)_viewport.Height, -25.0f, 25.0f);
	hRes = _pDevice->SetTransform(D3DTS_PROJECTION, &mProjectionMatrix);

	if (FAILED(hRes)) {
		Error::setError(6);
		return false;
	}

	//_pDevice->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_XRGB(80, 80, 80));			//ambient light
	_pDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
	//_pDevice->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_FLAT);
	//_pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);

	_pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE); // D3DCULL_CCW


															// TEMPORAL
	_pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
	_pDevice->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
	_pDevice->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);

	// Fijo valores por defecto para operaciones con el canal alpha
	_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	_pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	_pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	_pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	// Deshabilito los speculars
	_pDevice->SetRenderState(D3DRS_SPECULARENABLE, FALSE);

	// Deshabilito el stencil
	_pDevice->SetRenderState(D3DRS_STENCILENABLE, FALSE);

	return true;
}
