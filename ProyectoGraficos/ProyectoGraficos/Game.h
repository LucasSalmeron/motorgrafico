using namespace std;
#include "Window.h"
class Game {
public:
	Game();
	bool Initialize(HINSTANCE hInstance);
	void Loop();
	bool Shutdown();
protected:
	bool OnInit();
	bool OnUpdate();
	void OnDraw();
	bool OnShutDown();

};