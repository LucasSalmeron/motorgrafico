// ---------------------------------------------------------------------
// Archivo: main.cpp
// Desc: Ejemplo de creaci�n de Windows
//
// Autor: Juan Pablo (McKrackeN) Bettini
// ---------------------------------------------------------------------
#include "Game.h"
using namespace std;

// ---------------------------------------------------------------------
// WinMain
// Es el equivalente a la cl�sica funci�n int main ();
//Un cambio real
// ---------------------------------------------------------------------
int APIENTRY WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow)
{



	string desc;
	Game game;
	if (!game.Initialize(hInstance)) {
		return 1;
	}

	game.Loop();

	if (!game.Shutdown()) {
		return 1;
	}

	return 0;
}

